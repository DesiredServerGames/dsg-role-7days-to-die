﻿Configuration Push-7DaysToDieServer
{
param(
        [Parameter(Mandatory=$True)][ValidateNotNullorEmpty()][String]$NameServer,
        [Parameter(Mandatory=$False)][String]$ServerDescription,
        [Parameter(Mandatory=$False)][String]$WebsiteURL,
        [Parameter(Mandatory=$False)][String]$ServerPassword,
        [Parameter(Mandatory=$False)][ValidateRange(26900, 26905)][int]$ServerPort = "26900",
        [Parameter(Mandatory=$False)][ValidateRange(0, 3)][int]$ServerVisibility = '2',
        [Parameter(Mandatory=$False)][int]$MaxPlayer = '8',
        [Parameter(Mandatory=$False)][int]$AdminSlots = '0',
        [Parameter(Mandatory=$False)][int]$AdminSlotsPermission = '0',
        [Parameter(Mandatory=$False)][int]$ReservedSlots = '0',
        [Parameter(Mandatory=$False)][int]$RequireAdminSlotsPermission = '100',
        [Parameter(Mandatory=$False)][ValidateNotNullorEmpty()][ValidateSet("true","false" )][String]$ControlPanel = 'true',        
        [Parameter(Mandatory=$False)][int]$PanelPort = '8080',
        [Parameter(Mandatory=$False)][String]$ControlPanelPassword,                   
        [Parameter(Mandatory=$False)][ValidateNotNullorEmpty()][ValidateSet("true","false" )][String]$TelnetEnabled = 'false',
        [Parameter(Mandatory=$False)][int]$TelnetPort = '8081',
        [Parameter(Mandatory=$False)][String]$TelnetPassword,
        [Parameter(Mandatory=$False)][int]$TelnetFailedLoginLimit = '10',
        [Parameter(Mandatory=$False)][int]$TelnetFailedLoginsBlocktime = '10',
        [Parameter(Mandatory=$False)][ValidateNotNullorEmpty()][ValidateSet("true","false" )][String]$ShowTerminal = 'true',
        [Parameter(Mandatory=$False)][ValidateNotNullorEmpty()][ValidateSet("true","false" )][String]$AntiCheat = 'true',
        [Parameter(Mandatory=$False)][ValidateNotNullorEmpty()][ValidateSet("true","false" )][String]$PersistentPlayerProfiles = 'true',
        [Parameter(Mandatory=$False)][ValidateNotNullorEmpty()][String]$DirGameWorld = 'Navezgane',
        [Parameter(Mandatory=$False)][ValidateNotNullorEmpty()][String]$WorldGenSeed = 'asdf',
        [Parameter(Mandatory=$False)][ValidateRange(2048, 16384)][int]$WorldGenSize = '6144',        
        [Parameter(Mandatory=$False)][ValidateNotNullorEmpty()][String]$GameName = 'GameName',
        [Parameter(Mandatory=$False)][ValidateNotNullorEmpty()][String]$GameMode = 'GameModeSurvival',
        [Parameter(Mandatory=$False)][ValidateRange(0, 5)][int]$GameDifficulty = '2',
        [Parameter(Mandatory=$False)][ValidateRange(0, 100)][int]$BlockDamagePlayer = '100',                      
        [Parameter(Mandatory=$False)][ValidateRange(0, 100)][int]$BlockDamageAI = '100',
        [Parameter(Mandatory=$False)][ValidateRange(0, 100)][int]$BlockDamageAIBM = '100',
        [Parameter(Mandatory=$False)][ValidateRange(0, 100)][int]$XPMultiplier = '100',        
        [Parameter(Mandatory=$False)][int]$PlayerSafeZoneLevel = '5',   
        [Parameter(Mandatory=$False)][ValidateRange(0, 24)][int]$PlayerSafeZoneHours = '5',
        [Parameter(Mandatory=$False)][ValidateNotNullorEmpty()][ValidateSet("true","false" )][String]$BuildCreate = 'true',                   
        [Parameter(Mandatory=$False)][ValidateRange(0, 60)][int]$DayNightLength = '60',        
        [Parameter(Mandatory=$False)][ValidateRange(0, 24)][int]$DayLightLength = '60',         
        [Parameter(Mandatory=$False)][ValidateRange(0, 4)][int]$DropOnDeath = '1',
        [Parameter(Mandatory=$False)][ValidateRange(0, 3)][int]$DropOnQuit = '3',
        [Parameter(Mandatory=$False)][int]$BedrollDeadZoneSize = '15',
        [Parameter(Mandatory=$False)][int]$BedrollExpiryTime = '45',
        [Parameter(Mandatory=$False)][int]$MaxSpawnedZombies = '64',
        [Parameter(Mandatory=$False)][int]$MaxSpawnedAnimals = '50',
        [Parameter(Mandatory=$False)][int]$ServerMaxAllowedViewDistance = '12',
        [Parameter(Mandatory=$False)][ValidateNotNullorEmpty()][ValidateSet("true","false" )][String]$EnemySpawnMode = 'true',                   
        [Parameter(Mandatory=$False)][ValidateRange(0, 1)][int]$EnemyDifficulty = '0',
        [Parameter(Mandatory=$False)][ValidateRange(0, 3)][int]$ZombieFeralSense = '0',
        [Parameter(Mandatory=$False)][ValidateRange(0, 4)][int]$ZombieMove = '0',
        [Parameter(Mandatory=$False)][ValidateRange(0, 4)][int]$ZombieMoveNight = '3',
        [Parameter(Mandatory=$False)][ValidateRange(0, 4)][int]$ZombieFeralMove = '3',
        [Parameter(Mandatory=$False)][ValidateRange(0, 4)][int]$ZombieBMMove = '3',
        [Parameter(Mandatory=$False)][ValidateRange(0, 31)][int]$BloodMoonFrequency = '7',
        [Parameter(Mandatory=$False)][int]$BloodMoonRange = '0',
        [Parameter(Mandatory=$False)][ValidateRange(-1, 24)][int]$BloodMoonWarning = '8',
        [Parameter(Mandatory=$False)][int]$BloodMoonEnemyCount = '8',
        [Parameter(Mandatory=$False)][int]$LootAbundance = '100',
        [Parameter(Mandatory=$False)][ValidateRange(0, 31)][int]$LootRespawnDays = '7',
        [Parameter(Mandatory=$False)][int]$AirDropFrequency = '72',
        [Parameter(Mandatory=$False)][ValidateNotNullorEmpty()][ValidateSet("true","false" )][String]$AirDropMarker = 'true',
        [Parameter(Mandatory=$False)][ValidateRange(0, 3)][int]$PlayerKillingMode = '3',
        [Parameter(Mandatory=$False)][int]$LandClaimCount = '1',
        [Parameter(Mandatory=$False)][int]$LandClaimSize = '41',
        [Parameter(Mandatory=$False)][int]$LandClaimDeadZone = '30',
        [Parameter(Mandatory=$False)][int]$LandClaimExpiryTime = '7',
        [Parameter(Mandatory=$False)][int]$LandClaimDecayMode = '0',
        [Parameter(Mandatory=$False)][int]$LandClaimOnlineDurabilityModifier = '4',
        [Parameter(Mandatory=$False)][int]$LandClaimOfflineDurabilityModifier = '4',
        [Parameter(Mandatory=$False)][int]$LandClaimOfflineDelay = '0',
        #[Parameter(Mandatory=$False)][ValidateRange(0, 100)][int]$TwitchPermission = '90',
        #[Parameter(Mandatory=$False)][ValidateNotNullorEmpty()][ValidateSet("true","false" )][String]$TwitchBloodMoonAllowed = 'true',
        [Parameter(Mandatory=$True)][ValidateNotNullorEmpty()][int64]$SteamIDuserAdmin,
        [Parameter(Mandatory=$True)][ValidateNotNullorEmpty()][String]$NameUserAdmin


    )
$DscResourceDir = $PSScriptRoot
$DscResourceDir = (Get-Item $DscResourceDir).parent.FullName + "\DSCRessource"
$ServerPath = 'c:\dsgames\dsg-server\7DaysToDie'

Import-DscResource -ModuleName 'PSDesiredStateConfiguration', 'cFirewall', 'xXMLConfigFile'

    Node localhost
    {
#region Install        
        File Directory7DaysToDie {
            Type            = 'Directory'
            DestinationPath = $ServerPath
            Ensure          = 'Present'
        }
        Log BeforeInstall7DaysToDie {
            Message = "Téléchargement en cours de 7 Days to die Dedicated Server"
            DependsOn = "[File]Directory7DaysToDie"
        }
        Script Install7DaysToDie{           
            SetScript = {
                Start-Process -FilePath "c:\dsgames\dsg-tools\steamcmd\Steamcmd.exe" -ArgumentList "+login anonymous +force_install_dir ""c:\dsgames\dsg-server\7DaysToDie"" +app_update 294420 validate +exit" -Wait -PassThru -NoNewWindow;
            }
            TestScript = { 
                Test-Path "$ServerPath\7DaysToDieServer.exe"
            }
            GetScript = { 
                @{ Result = (Test-Path "$ServerPath\7DaysToDieServer.exe") } 
            }
        }
        Log AfterInstall7DaysToDie {
            Message = "Téléchargement terminé de 7 Days to die Dedicated Server"
            DependsOn = "[Script]Install7DaysToDie"
        }
#endregion
#region Configure FireWall       
        cFirewallRule 7DaysToDie-TCP {
            Name        = '7DaysToDie-TCP'
            Description = '7DaysToDie-server (TCP-in)'
            Action      = 'Allow'
            Direction   = 'Inbound'
            LocalPort   = ('26900','8080-8081')
            Protocol    = 'TCP'
            Profile     = 'All'
            Enabled     = $True
            Ensure      = 'Present'
        }
        cFirewallRule 7DaysToDie-UDP {
            Name        = '7DaysToDie-UDP'
            Description = '7DaysToDie-server (UDP-in)'
            Action      = 'Allow'
            Direction   = 'Inbound'
            LocalPort   = ('26900-26905','27015-27020')
            Protocol    = 'UDP'
            Profile     = 'All'
            Enabled     = $True
            Ensure      = 'Present'
        }
#endregion
#region Copy File Configuration
        File CopyServerConFig {
            Type            = 'File'
            SourcePath      = "$DscResourceDir\serverconfig.xml"
            DestinationPath = "$ServerPath\serverconfig.xml"
            Ensure          = "Present"
            Force           = $true
            Checksum        = "SHA-256"
        }
        File CopyAdminConFig {
            Type            = 'File'
            SourcePath      = "$DscResourceDir\serveradmin.xml"
            DestinationPath = "$ServerPath\serveradmin.xml"
            Ensure          = "Present"
        }        
#endregion Copy File Configuration
#region Configure Server Parameter
    #region GENERAL SERVER SETTINGS
        #region Server representation        
        XMLConfigFile ServerName {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ServerName'
            Value       = "$NameServer"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        XMLConfigFile ServerDescription {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ServerDescription'
            Value       = "Serveur cree avec les script DesiredServerGames - $ServerDescription"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        XMLConfigFile WebsiteURL {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ServerWebsiteURL'
            Value       = "$WebsiteURL"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        XMLConfigFile ServerPassword {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ServerPassword'
            Value       = "$ServerPassword"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        #endregion Server representation
        #region Networking       
        XMLConfigFile ServerPort {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ServerPort'
            Value       = "$ServerPort"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        XMLConfigFile ServerVisibility {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ServerVisibility'
            Value       = "$ServerVisibility"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        #endregion Networking
        #region Slots                          
        XMLConfigFile MaxPlayer {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ServerMaxPlayerCount'
            Value       = "$MaxPlayer"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        XMLConfigFile AdminSlots {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ServerAdminSlots'
            Value       = "$AdminSlots"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        XMLConfigFile AdminSlotsPermission {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ServerAdminSlotsPermission'
            Value       = "$RequireAdminSlotsPermission"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        XMLConfigFile ReservedSlots {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ServerReservedSlots'
            Value       = "$ReservedSlots"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        XMLConfigFile ReservedSlotsPermission {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ServerReservedSlotsPermission'
            Value       = "$ReservedSlotsPermission"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        #endregion Slots
        #region Admin interfaces          
        XMLConfigFile ControlPanel {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ControlPanelEnabled'
            Value       = "$ControlPanel"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile PanelPort {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ControlPanelPort'
            Value       = "$PanelPort"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        } 
        XMLConfigFile ControlPanelPassword {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ControlPanelPassword'
            Value       = "$ControlPanelPassword"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        } 
        XMLConfigFile TelnetEnabled {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'TelnetEnabled'
            Value       = "$TelnetEnabled"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        if($TelnetEnabled -eq 'true'){
            $osInfo = Get-CimInstance -ClassName Win32_OperatingSystem
            if($osInfo.ProductType -eq "1"){
                Enable-WindowsOptionalFeature -Online -FeatureName TelnetClient -All
            }
            Else {
                WindowsFeature Telnet-Client {
                    Ensure = "Present"
                    Name = "Telnet-Client"
                }
            }    
        }
        XMLConfigFile TelnetPort {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'TelnetPort'
            Value       = "$TelnetPort"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        } 
        XMLConfigFile TelnetPassword {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'TelnetPassword'
            Value       = "$TelnetPassword"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        } 
        XMLConfigFile TelnetFailedLoginLimit {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'TelnetFailedLoginLimit'
            Value       = "$TelnetFailedLoginLimit"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        } 
        XMLConfigFile TelnetFailedLoginsBlocktime {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'TelnetFailedLoginsBlocktime'
            Value       = "$TelnetFailedLoginsBlocktime"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }    
        XMLConfigFile ShowTerminal {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'TerminalWindowEnabled'
            Value       = "$ShowTerminal"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        #endregion Admin interfaces
        #region Save Folder
        XMLConfigFile SaveGameFolder {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'SaveGameFolder'
            Value       = "$ServerPath"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        } 
        #endregion Save Folder
        #region technical settings     
        XMLConfigFile AntiCheat {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'EACEnabled'
            Value       = "$AntiCheat"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile PersistentPlayerProfiles {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'PersistentPlayerProfiles'
            Value       = "$PersistentPlayerProfiles"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        #endregion technical settings
    #endregion GENERAL SERVER SETTINGS          
    #region GAMEPLAY
        #region World     
        XMLConfigFile DirGameWorld {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'GameWorld'
            Value       = "$DirGameWorld"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile WorldGenSeed {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'WorldGenSeed'
            Value       = "$WorldGenSeed"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile WorldGenSize {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'WorldGenSize'
            Value       = "$WorldGenSize"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile GameName {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'GameName'
            Value       = "DSG-$GameName"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile GameMode {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'GameMode'
            Value       = "$GameMode"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        #endregion World
        #region Difficulty
        XMLConfigFile GameDifficulty {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'GameDifficulty'
            Value       = "$GameDifficulty"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile BlockDamagePlayer {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'BlockDamagePlayer'
            Value       = "$BlockDamagePlayer"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile BlockDamageAI {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'BlockDamageAI'
            Value       = "$BlockDamageAI"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }    
        XMLConfigFile BlockDamageAIBM {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'BlockDamageAIBM'
            Value       = "$BlockDamageAIBM"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile XPMultiplier {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'XPMultiplier'
            Value       = "$XPMultiplier"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile PlayerSafeZoneLevel {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'PlayerSafeZoneLevel'
            Value       = "$PlayerSafeZoneLevel"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile PlayerSafeZoneHours {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'PlayerSafeZoneHours'
            Value       = "$PlayerSafeZoneHours"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile BuildCreate {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'BuildCreate'
            Value       = "$BuildCreate"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile DayNightLength {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'DayNightLength'
            Value       = "$DayNightLength"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile DayLightLength {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'DayLightLength'
            Value       = "$DayLightLength"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile DropOnDeath {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'DropOnDeath'
            Value       = "$DropOnDeath"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile DropOnQuit {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'DropOnQuit'
            Value       = "$DropOnQuit"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile BedrollDeadZoneSize {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'BedrollDeadZoneSize'
            Value       = "$BedrollDeadZoneSize"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile BedrollExpiryTime {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'BedrollExpiryTime'
            Value       = "$BedrollExpiryTime"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        #endregion Difficulty  
        #region PVE Settings
        XMLConfigFile MaxSpawnedZombies {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'MaxSpawnedZombies'
            Value       = "$MaxSpawnedZombies"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile MaxSpawnedAnimals {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'MaxSpawnedAnimals'
            Value       = "$MaxSpawnedAnimals"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile ServerMaxAllowedViewDistance {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ServerMaxAllowedViewDistance'
            Value       = "$ServerMaxAllowedViewDistance"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile EnemySpawnMode {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'EnemySpawnMode'
            Value       = "$EnemySpawnMode"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile EnemyDifficulty {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'EnemyDifficulty'
            Value       = "$EnemyDifficulty"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile ZombieFeralSense {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ZombieFeralSense'
            Value       = "$ZombieFeralSense"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile ZombieMove {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ZombieMove'
            Value       = "$ZombieMove"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile ZombieMoveNight {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ZombieMoveNight'
            Value       = "$ZombieMoveNight"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile ZombieFeralMove {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ZombieFeralMove'
            Value       = "$ZombieFeralMove"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile ZombieBMMove {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'ZombieBMMove'
            Value       = "$ZombieBMMove"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile BloodMoonFrequency {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'BloodMoonFrequency'
            Value       = "$BloodMoonFrequency"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile BloodMoonRange {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'BloodMoonRange'
            Value       = "$BloodMoonRange"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile BloodMoonWarning {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'BloodMoonWarning'
            Value       = "$BloodMoonWarning"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile BloodMoonEnemyCount {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'BloodMoonEnemyCount'
            Value       = "$BloodMoonEnemyCount"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        #endregion PVE Settings
        #region Loot
        XMLConfigFile LootAbundance {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'LootAbundance'
            Value       = "$LootAbundance"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile LootRespawnDays {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'LootRespawnDays'
            Value       = "$LootRespawnDays"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile AirDropFrequency {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'AirDropFrequency'
            Value       = "$AirDropFrequency"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        XMLConfigFile AirDropMarker {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'AirDropMarker'
            Value       = "$AirDropMarker"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }        
        #endregion Loot 
        #region Multiplayer
        XMLConfigFile PlayerKillingMode {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'PlayerKillingMode'
            Value       = "$PlayerKillingMode"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        #endregion Multiplayer
        #region Land claim  
        XMLConfigFile LandClaimCount {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'LandClaimCount'
            Value       = "$LandClaimCount"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile LandClaimSize {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'LandClaimSize'
            Value       = "$LandClaimSize"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile LandClaimDeadZone {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'LandClaimDeadZone'
            Value       = "$LandClaimDeadZone"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile LandClaimExpiryTime {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'LandClaimExpiryTime'
            Value       = "$LandClaimExpiryTime"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile LandClaimDecayMode {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'LandClaimDecayMode'
            Value       = "$LandClaimDecayMode"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }  
        XMLConfigFile LandClaimOnlineDurabilityModifier {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'LandClaimOnlineDurabilityModifier'
            Value       = "$LandClaimOnlineDurabilityModifier"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }                                                                                                                                                                          
        XMLConfigFile LandClaimOfflineDurabilityModifier {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'LandClaimOfflineDurabilityModifier'
            Value       = "$LandClaimOfflineDurabilityModifier"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        } 
        XMLConfigFile LandClaimOfflineDelay {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'LandClaimOfflineDelay'
            Value       = "$LandClaimOfflineDelay"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        #endregion Land claim
<#        #region Twitch 
        XMLConfigFile TwitchPermission {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'TwitchPermission'
            Value       = "$TwitchPermission"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        } 
        XMLConfigFile TwitchBloodMoonAllowed {
            ConfigPath  = "$ServerPath\serverconfig.xml"
            XPath       = '//ServerSettings/property'
            Name        = 'TwitchBloodMoonAllowed'
            Value       = "$TwitchBloodMoonAllowed"
            Attribute1  = "name"
            Attribute2  = "value"
            isAttribute = $false
            Ensure      = 'Present'
        }
        #endregion Twitch #>
    #endregion GAMEPLAY 
#endregion Configure Server Parameter
#region Configure Server Admin
        XMLConfigFile SteamIDuserAdmin {
            ConfigPath  = "$ServerPath\serveradmin.xml"
            XPath       = '//adminTools/admins/user'
            Name        = 'steamID'
            Value       = "$SteamIDuserAdmin"
            isAttribute = $true
            Ensure      = 'Present'
        }
        XMLConfigFile NameUserAdmin {
            ConfigPath  = "$ServerPath\serveradmin.xml"
            XPath       = '//adminTools/admins/user'
            Name        = 'name'
            Value       = "$NameUserAdmin"
            isAttribute = $true
            Ensure      = 'Present'
        }        
#endregion Configure Server Admin

    }
}

Push-7DaysToDieServer -OutputPath $PSScriptRoot -NameServer "test" `
                                                -ServerDescription "Mon serveur test" `
                                                -ServerPassword "toto" `
                                                -SteamIDuserAdmin "76561197968610918" `
                                                -NameUserAdmin "Jugem" `
                                                -ControlPanelPassword "toto" `
                                                -AntiCheat "false"